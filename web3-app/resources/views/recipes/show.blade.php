@extends('layouts.app')

@section('title', 'Recipe')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <img class="card-img-top" src="/uploads/recipes/{{$recipe->image}}" alt="{{$recipe->name}}">
                <h2>{{$recipe->name}}</h2>
                <p>{{$recipe->instructions}}</p>
                <a href="/profile/{{$recipe->user->id}}#user-recipes">More recipes by {{$recipe->user->name}}</a>
            </div>
            <div class="col-md-8">
                @can('update', $recipe)
                    <a class="btn btn-primary float-left" href="/recipe/{{$recipe->id}}/edit">Edit</a>
                @endcan
                @can('delete', $recipe)
                    <a class="btn btn-danger float-right"
                       href="/recipe/{{$recipe->id}}"
                       onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                        Delete
                    </a>

                    <form id="delete-form" action="/recipe/{{$recipe->id}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </div>
        </div>
    </div>
@endsection
