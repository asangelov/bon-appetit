<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
{{--    <link href="{{ base_path() }}/public/css/custom.css" rel="stylesheet">--}}
</head>
<body>
<h1>All recipes from {{ config('app.name') }}</h1>
<div class="wrapper">
    @foreach($recipes->reverse() as $recipe)
        <div>
            <img src="{{ base_path() }}/public/uploads/recipes/{{ $recipe->image }}" alt="{{$recipe->name}}">
            <div>
                <h5><a href="{{ url('/') }}/recipe/{{$recipe->id}}">{{ $recipe->name }}</a></h5>
                <p>
                    {{ $recipe->instructions }}</p>
            </div>
            <div>
                <small>By <a href="{{ url('/') }}/profile/{{$recipe->user->id}}">{{ $recipe->user->name }}</a></small>
            </div>
        </div>
    @endforeach
</div>
</body>
</html>