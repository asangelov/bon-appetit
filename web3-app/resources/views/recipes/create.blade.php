@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add a new recipe</div>

                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data" action="/recipe">
                            @csrf

                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Image</label>

                                <div class="col-md-6">
                                    <input
                                            type="file"
                                            class="form-control-file"
                                            name="image" id="image"
                                            aria-describedby="fileHelp">
                                    <small
                                            id="fileHelp"
                                            class="form-text text-muted"
                                    >Please upload a valid image file. Size of image should not be more than 2MB.</small>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                                <div class="col-md-6">
                                    <input
                                        id="name"
                                        type="text"
                                        class="form-control"
                                        name="name"
                                        value="{{ old('name') }}"
                                        required autofocus>
                                </div>

                                @error('name')
                                <p class="text-danger">This field is required</p>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <label for="instructions" class="col-md-4 col-form-label text-md-right">Instructions</label>

                                <div class="col-md-6">
                                    <textarea
                                        id="instructions"
                                        type="text" class="form-control"
                                        name="instructions"
                                        value=""
                                        required
                                    >{{ old('instructions') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
