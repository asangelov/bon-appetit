@extends('layouts.app')

@section('head')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        @can('update', $user, $user)
                            Your Profile
                        @else
                            {{$user->name}}'s Profile
                        @endcan
                    </div>

                    <div class="profile-header-container">
                        <div class="profile-header-img">
                            <img class="rounded-circle" src="/uploads/avatars/{{ $user->avatar }}" />
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Name: {{$user->name}}
                        @can ('update', $user)
                            <p> Email: {{$user->email}}</p>
                                <a class="btn btn-primary float-left" href="/profile/{{$user->id}}/edit">Edit</a>
                        @endcan
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-header" id="user-recipes">
                        @can('update', $user, $user)
                            Your Recipes
                        @else
                            {{$user->name}}'s Latest 5 Recipes
                        @endcan
                    </div>

                    @if(count($user->recipes) > 0)
                        <div class="card-body">
                            @foreach($user->recipes->reverse()->take(5) as $recipe)
                                <div class="d-flex recipe-card m-2">
                                    <a href="/recipe/{{ $recipe->id }}" class="recipe-card-link">
                                        <div class="card card-home">
                                            <img class="card-img-top" src="/uploads/recipes/{{ $recipe->image }}" alt="{{ $recipe->name }}">
                                            <div class="card-body">
                                                <h5 class="card-title">{{ $recipe->name }}</h5>
                                                <p class="card-text">
                                                    {{ count(explode(' ', $recipe->instructions)) < 25 ? implode(' ', array_slice(explode(' ', $recipe->instructions), 0, 25)) : implode(' ', array_slice(explode(' ', $recipe->instructions), 0, 25)) . '...'}}</p>

                                            </div>
                                            <div class="card-footer">
                                                <small class="text-muted">By {{ $recipe->user->name }}</small>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="card-body">
                            <p class="m-0">{{ $user->name }} hasn't posted any recipes yet :(</p>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
@endsection
