<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use PDF;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recipes = Recipe::all();

        return view('recipes.index', compact('recipes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            return view('recipes.create');
        }
        else {
            return redirect('/login');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = 'default.svg';

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->fit(900, 400)
                ->insert(public_path('/uploads/watermark.png'), 'bottom-right', 10, 10)
                ->save(public_path('/uploads/recipes/') . $filename);
        }

        request()->validate([
            'name' => 'required|min:3|max:255',
            'instructions' => 'required'
        ]);

        Recipe::create([
            'user_id' => request()->user()->id,
            'name' => request('name'),
            'instructions' => request('instructions'),
            'image' => $filename,
        ]);

        return redirect('/recipes');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        return view('recipes.show', compact('recipe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        return view('recipes.edit', compact('recipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        $this->authorize('update', $recipe);
        $recipe->update(request()->validate([
            'name' => 'required',
            'instructions' => 'required'
        ]));

        return redirect('/recipe/' . $recipe->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        $this->authorize('delete', $recipe);
        $recipe->delete();

        return redirect('recipes');

    }

    public function export(){
        $recipes = Recipe::all();
        $pdf = PDF::setOptions(['isRemoteEnabled' => true])
            ->loadView('recipes.pdf', compact('recipes'));

        return $pdf->download('export.pdf');
    }
}
